#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "point2d.hpp"

class Polygon {
public:
    typedef std::vector<Point2D>   Points;
    typedef Points::value_type     value_type;
    typedef Points::const_iterator const_iterator;
    typedef Points::iterator       iterator;

    Polygon() {}
    Polygon( std::initializer_list<Point2D> init ) : m_points( init ) {}

    template<typename Iter>
    Polygon( Iter b, Iter e ) : m_points( b, e ) {}

    bool isClockWise() const;
    double area() const;
    bool isInside( const Polygon &p ) const;
    bool isInside( const Point2D &p ) const;

    const Point2D &front() const { return m_points.front(); }
    const Point2D &back() const { return m_points.back(); }
    const_iterator begin() const { return m_points.begin(); }
    const_iterator end() const { return m_points.end(); }
    size_t size() const { return m_points.size(); }
    const Point2D &operator[]( size_t n ) const { return m_points[n]; }
    bool empty() const { return size() == 0; }
    bool operator==( const Polygon &n ) const { return this->m_points == n.m_points; }
    bool operator<( const Polygon &n ) const { return this->m_points < n.m_points; }

    template<typename... Args>
    void emplace_back( Args&&... args )
    {
        m_area = 0;
        m_points.emplace_back( std::forward<Args>( args )... );
    }

    void push_back( const Point2D &p )
    {
        m_area = 0;
        m_points.push_back( p );
    }

    void push_back( Point2D &&p )
    {
        m_area = 0;
        m_points.push_back( std::move( p ) );
    }

    iterator erase( const_iterator pos )
    {
        m_area = 0;
        return m_points.erase( pos );
    }

    iterator erase( const_iterator first, const_iterator last )
    {
        m_area= 0;
        return m_points.erase( first, last );
    }

    template<typename Iter>
    iterator insert( const_iterator pos, Iter first, Iter last )
    {
        m_area = 0;
        return m_points.insert( pos, first, last );
    }

    Point2D &operator[]( size_t n ) {
        m_area = 0;
        return m_points[n];
    }

private:
    Points         m_points;
    mutable double m_area;
};

typedef std::vector<Polygon> Polygons;

std::ostream &operator<<( std::ostream &out, const Polygon &p );
std::ostream &operator<<( std::ostream &out, const Polygons &ps );


#endif // POLYGON_HPP
