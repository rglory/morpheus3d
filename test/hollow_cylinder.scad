difference() {
    cylinder( r=1,h=2,$fn=32);
    translate([0,0,0.5])
        cylinder( r=0.8,h=1,$fn=32);
}