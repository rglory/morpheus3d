#include "point3d.hpp"

Point3D &Point3D::operator+=( const Point3D &p )
{
    m_x += p.m_x;
    m_y += p.m_y;
    m_y += p.m_y;
    return *this;
}

Point3D &Point3D::operator-=( const Point3D &p )
{
    m_x -= p.m_x;
    m_y -= p.m_y;
    m_y -= p.m_y;
    return *this;
}

Point3D operator-( const Point3D &p1, const Point3D &p2 )
{
    return Point3D{ p1.x() - p2.x(), p1.y() - p2.y(), p1.z() - p2.z() };
}

Point3D operator*( const Point3D &a, const Point3D &b )
{
    return Point3D{ a.y() * b.z() - a.z() * b.y(),
                    a.z() * b.x() - a.x() * b.z(),
                    a.x() * b.y() - a.y() * b.x() };
}


bool operator<( const Point3D &p1, const Point3D &p2 )
{
    auto sign = dcmp( p1.x(), p2.x() );
    if( sign ) return sign < 0;
    sign = dcmp( p1.y(), p2.y() );
    if( sign ) return sign < 0;
    return dcmp( p1.z(), p2.z() ) < 0;
}

bool operator<=( const Point3D &p1, const Point3D &p2 )
{
    if( dcmp( p1.x(), p2.x() ) > 0 ) return false;
    if( dcmp( p1.y(), p2.y() ) > 0 ) return false;
    return dcmp( p1.z(), p2.z() ) <= 0;
}

bool operator>( const Point3D &p1, const Point3D &p2 )
{
    auto sign = dcmp( p1.x(), p2.x() );
    if( sign ) return sign > 0;
    sign = dcmp( p1.y(), p2.y() );
    if( sign ) return sign > 0;
    return dcmp( p1.z(), p2.z() ) > 0;
}

bool operator>=( const Point3D &p1, const Point3D &p2 )
{
    if( dcmp( p1.x(), p2.x() ) < 0 ) return false;
    if( dcmp( p1.y(), p2.y() ) < 0 ) return false;
    return dcmp( p1.z(), p2.z() ) >= 0;
}

bool operator==( const Point3D &p1, const Point3D &p2 )
{
    if( dcmp( p1.x(), p2.x() ) ) return false;
    if( dcmp( p1.y(), p2.y() ) ) return false;
    return dcmp( p1.z(), p2.z() ) == 0;
}
