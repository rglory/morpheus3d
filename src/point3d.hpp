#ifndef POINT3D_HPP
#define POINT3D_HPP

#include "common.hpp"

#include <iostream>

class Point3D {
public:
    Point3D( double x, double y, double z ) : m_x{ x }, m_y{ y }, m_z{ z } {}
    Point3D() : Point3D{ 0.0, 0.0, 0.0 } {}

    double x() const { return m_x; }
    double y() const { return m_y; }
    double z() const { return m_z; }

    Point3D &operator+=( const Point3D &p );
    Point3D &operator-=( const Point3D &p );

private:
    double m_x;
    double m_y;
    double m_z;
};

bool operator<( const Point3D &p1, const Point3D &p2 );
bool operator<=( const Point3D &p1, const Point3D &p2 );
bool operator>( const Point3D &p1, const Point3D &p2 );
bool operator>=( const Point3D &p1, const Point3D &p2 );
bool operator==( const Point3D &p1, const Point3D &p2 );

Point3D operator-( const Point3D &p1, const Point3D &p2 );
Point3D operator*( const Point3D &p1, const Point3D &p2 );

inline
bool operator!=( const Point3D &p1, const Point3D &p2 )
{
    return !( p1 == p2 );
}

inline
std::ostream &operator<<( std::ostream &out, const Point3D &p )
{
    return out << '(' << dround( p.x() ) << ',' 
               << dround( p.y() ) << ',' 
               << dround( p.z() ) << ')';
}

inline
Point3D max( const Point3D &p1, const Point3D &p2 )
{
    return Point3D{ std::max( p1.x(), p2.x() ),
                    std::max( p1.y(), p2.y() ),
                    std::max( p1.z(), p2.z() ) };
}

inline
Point3D min( const Point3D &p1, const Point3D &p2 )
{
    return Point3D{ std::min( p1.x(), p2.x() ),
                    std::min( p1.y(), p2.y() ),
                    std::min( p1.z(), p2.z() ) };
}



#endif // POINT3D_HPP
