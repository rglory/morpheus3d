#include "polygon.hpp"

bool Polygon::isClockWise() const
{
    return m_area > 0;
}

double Polygon::area() const
{
    if( size() < 3 ) return 0;
    if( m_area == 0 ) {
        m_area = accumulate3( begin(), end(), 0.0, []( const auto &p1, const auto &p2, const auto &p3 ) {
            return p2.x() * ( p1.y() - p2.y() );
        } );
    }
    return std::abs( m_area );
}

bool Polygon::isInside( const Polygon &p ) const
{
    return !std::any_of( p.begin(), p.end(), [this]( const auto &pt ) { return !this->isInside( pt ); } );
}

bool Polygon::isInside( const Point2D &p ) const
{
    bool onEdge = false;
    auto check = [p,&onEdge] ( const auto &p1, const auto &p2 ) {
        if( dcmp( ( p1.y() - p2.y() ) * p.x() + ( p2.x() - p1.x() ) * p.y(),
                  p2.x() * p1.y() - p1.x() * p2.y() ) == 0 ) {
            if( dcmp( dist( p1, p ) + dist( p2, p ), dist( p1, p2 ) ) == 0 )
                return onEdge = true;
            return false;
        }
        if( dcmp( p.y(), p1.y() ) == 0 ) return dcmp( p2.y(), p.y() ) > 0 && dcmp( p1.x(), p.x() ) > 0;
        if( dcmp( p.y(), p2.y() ) == 0 ) return dcmp( p1.y(), p.y() ) > 0 && dcmp( p2.x(), p.x() ) > 0;
        if( dcmp( p.y(), std::max( p1.y(), p2.y() ) ) > 0 ) return false;
        if( dcmp( p.y(), std::min( p1.y(), p2.y() ) ) < 0 ) return false;
        double x = ( p.y() - p1.y() ) * ( p2.x() - p1.x() ) / ( p2.y() - p1.y() ) + p1.x();
        return dcmp( x, p.x() ) > 0;
    };

    auto count = accumulate2( begin(), end(), 0, check );
    /*if( debug ) {
        std::cout << "point " << p << " count " << count << " from " << *this << std::endl;
        apply2( begin(), end(), [check,p] ( const auto &p1, const auto &p2 ) {
            if( check( p1, p2 ) )
                std::cout << "checked p " << p << " in " << p1 << " - " << p2 << std::endl;
        } );
    }*/

    return onEdge || count % 2;
}


std::ostream &operator<<( std::ostream &out, const Polygon &p )
{
    out << "[";
    std::string del;
    for( const auto &pt : p ) {
        out << del << pt;
        if( del.empty() ) del = ", ";
    }
    return out << "] area " << p.area();
}

std::ostream &operator<<( std::ostream &out, const Polygons &ps )
{
    out << "{";
    for( const auto &p : ps )
        out << " " << p;
    return out << " }";
}
