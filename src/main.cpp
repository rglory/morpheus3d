#include <iostream>

#include "mesh.hpp"

int main()
{
    Mesh m;
    m.load( "test/pyramid.stl" );
    //m.print( std::cout );
    for( double z : { 0.0, 0.5, 1.0 } ) {
        std::cout << "z=" << z << std::endl;
        auto p = m.slice( z );
        for( const auto &pl : p ) {
            std::cout << "\tpolygon: ";
            for( const auto &pt : pl )
                std::cout << "(" << pt.x() << "," << pt.y() << ") ";
            std::cout << std::endl;
        }
    }
    return 0;
}

