#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "point3d.hpp"
#include "point2d.hpp"
#include "polygon.hpp"

#include <array>
#include <algorithm>

class Triangle {
public:
    typedef std::array<Point3D,3>      Array;
    typedef Array::iterator            iterator;
    typedef Array::const_iterator      const_iterator;

    template<class Iter>
    Triangle( Iter b, Iter e )
    {
        std::copy( b, e, m_array.begin() );
        calcNorm();
    }

    template<class Iter, class Convert>
    Triangle( Iter b, Iter e, Convert &&cnv )
    {
        std::transform( b, e, m_array.begin(), std::forward<Convert>( cnv ) );
        calcNorm();
    }

    Triangle();

    const Point3D &operator[]( size_t n ) const { return m_array[n]; }
    const Point3D &norm() const { return m_norm; }

    void set( size_t n, Point3D p )
    {
        m_array[n] = std::move( p );
        calcNorm();
    }

    void move( const Point3D &d );

    const_iterator begin() const { return m_array.begin(); }
    const_iterator end() const { return m_array.end(); }

    bool intersects( double z ) const;
    Polygon intersection(double z ) const;

private:
    std::array<int,3> signs( double z ) const;
    void calcNorm();

    Array   m_array;
    Point3D m_norm;
};

std::ostream &operator<<( std::ostream &out, const Triangle &t );

#endif // TRIANGLE_HPP
