#include "point2d.hpp"

#include <algorithm>

bool operator<( const Point2D &p1, const Point2D &p2 )
{
    auto sign = dcmp( p1.x(), p2.x() );
    if( sign ) return sign < 0;
    return dcmp( p1.y(), p2.y() ) < 0;
}

bool operator<=( const Point2D &p1, const Point2D &p2 )
{
    if( dcmp( p1.x(), p2.x() ) > 0 ) return false;
    return dcmp( p1.y(), p2.y() ) <= 0;
}

bool operator>( const Point2D &p1, const Point2D &p2 )
{
    auto sign = dcmp( p1.x(), p2.x() );
    if( sign ) return sign > 0;
    return dcmp( p1.y(), p2.y() ) > 0;
}


bool operator>=( const Point2D &p1, const Point2D &p2 )
{
    if( dcmp( p1.x(), p2.x() ) < 0 ) return false;
    return dcmp( p1.y(), p2.y() ) >= 0;
}

bool operator==( const Point2D &p1, const Point2D &p2 )
{
    if( dcmp( p1.x(), p2.x() ) ) return false;
    return dcmp( p1.y(), p2.y() ) == 0;
}
