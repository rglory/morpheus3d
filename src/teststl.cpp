#include <iostream>
#include <sstream>
#include <fstream>

#include "mesh.hpp"

int main( int argc, char **argv )
{
    if( argc < 3 ) {
        std::cout << "usage: " << argv[0] << " test.stl test.res" << std::endl;
        return 100;
    }

    int rez = 0;
    try {
        Mesh m;
        m.load( argv[1] );
        std::ifstream res( argv[2] );
        for( double z = m.min().z(); z <= m.max().z(); z+=0.5 ) {
            std::ostringstream os;
            os <<  "z=" << dround( z );
            auto p = m.slice( z );
            for( const auto &pl : p ) {
                os << " polygon: [";
                for( const auto &pt : pl )
                    os << pt;
                os << "]";
            }
            std::string sres;
            if( !std::getline( res, sres ) )
                throw std::runtime_error( "unexpected end of result file" );
            
            if( os.str() != sres ) {
                std::cerr << "error: expected \"" << sres << "\"" << std::endl;
                std::cerr << "            got \"" << os.str() << "\"" << std::endl;
                rez = 102;
            }

        }
    }
    catch( const std::exception &ex )
    {
        std::cerr << "error: " << ex.what() << std::endl;
        return 101;
    }
    return rez;
}

