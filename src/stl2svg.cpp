#include <iostream>
#include <fstream>

#include "mesh.hpp"

int main( int argc, char **argv )
{
    if( argc < 2 ) {
        std::cout << "usage: " << argv[0] << " [-s step] input.stl [output.html]" << std::endl;;
        return 100;
    }
    double step = 0.5;
    std::vector<std::string> params;
    for( int i = 1; i < argc; ++i ) {
        if( std::string( argv[i] ) == "-s" ) {
            if( ++i == argc ) {
                std::cerr << "error: expected argument for -s" << std::endl;
                return 101;
            }
            step = std::stod( argv[i] );
            if( step <= 0 ) {
                std::cerr << "invalid step:" << step << " expected > 0" << std::endl;
                return 102;
            }
            continue;
        }
        if( params.size() == 2 ) {
            std::cerr << "error: too many parameters, 2 expected" << std::endl;
            return 103;
        }
        params.emplace_back( argv[i] );
    }
    std::string out;
    if( params.size() == 1 ) {
        auto pos = params[0].find_last_of( "." );
        out = params[0].substr( 0, pos ) + ".html";
    } else
        out = params[1];

    std::cout << "processing " << params[0] << "output " << out << " with step " << step << std::endl;
    try {
        Mesh m;
        m.load( params[0] );
        auto min = m.min();
        auto max = m.max();
        std::cout << "loaded " << m.size() << " triangles from " << min << " to " << max << std::endl;

        const int xres = 800;
        const int yres = 600;
        double xk = ( 790.0 - 10.0 ) / ( max.x() - min.x() );
        double yk = ( 590.0 - 20.0 ) / ( max.y() - min.y() );
        double k = std::min( xk, yk );
        double xc = 10 - min.x() * k;
        double yc = 20 - min.y() * k;

        int count = 0;
        std::ofstream svg( out );
        svg << "<!CTYPE html>\n<html>\n<body>\n\n<svg width=\"" << xres << "\" height=\"" << yres << "\" id=\"svg\">" << std::endl;
        svg << "    <g style=\"stroke:black;stroke-width:1;fill:green;fill-rule:evenodd;\" >" << std::endl;

        double end = m.max().z();
        int row = 0;
        for( double z = m.min().z(); z <= end; z+= step ) {
            svg << "        <g id=\"g" << row++ << "\" visibility=\"hidden\">" << std::endl;
            svg << "            <text x=\"400\" y=\"10\" fill=\"black\">Z=" << dround(z) << "</text>" << std::endl;
            svg << "            <path d=\"";
            auto ps = m.slice( z + step / 2.0 );
            std::string circles;
            std::string arrpath;
            bool first = true;
            for( const auto &pl : ps ) {
                bool first = true;
                Point2D prev = pl.back();
                prev = Point2D( prev.x() * k + xc, yres + 10 - ( prev.y() * k + yc ) );
                for( const auto &pt : pl ) {
                    Point2D tpt( pt.x() * k + xc, yres + 10 - ( pt.y() * k + yc ) );
                    if( first ) {
                        first = false;
                        circles += "            <circle cx=\"" + std::to_string( dround( tpt.x() ) ) +
                                                    "\" cy=\"" + std::to_string( dround( tpt.y() ) ) +
                                   "\" r=\"3\" fill=\"black\"/>\n";
                        svg << "M ";
                    } else {
                        svg << " L ";
                    }
                    const double length = 9;
                    if( dist( tpt, prev ) > length ) {
                        if( !arrpath.empty() )
                            arrpath += " ";
                        arrpath += "M " + std::to_string( dround( tpt.x() ) ) + " " + std::to_string( dround( tpt.y() ) );
                        double l = ( prev - tpt ).atan2();
                        for( double di : { -1, 1 } ) {
                            double ln = l + di * 0.2;
                            Point2D npt( tpt.x() + ::cos( ln ) * length, tpt.y() + ::sin( ln ) * length );
                            arrpath += " L " + std::to_string( dround( npt.x() ) ) + " " + std::to_string( dround( npt.y() ) );
                        }
                        arrpath += " z";
                    }
                        
                    svg << dround( tpt.x() ) << " " << dround( tpt.y() );
                    prev = tpt;
                }
                svg << " z ";
                ++count;
            }
            svg << "\" />" << std::endl;
            svg << "            <path fill=\"black\" d=\"" << arrpath << "\"/>" << std::endl;
            svg << circles;
            svg << "        </g>" << std::endl;
        }
        svg << "    </g>\n</svg>" << std::endl;
        std::cout << "output: " << count << " polygons" << std::endl;
        svg << 
        "<script>\n"
        "    var svg = document.getElementById( \"svg\" );\n"
        "    var maxIndex = " << row - 1 << ";\n"
        "    var currIndex = 0\n\n"
        "    var layers = svg.children[0].children;\n"
        "    layers[currIndex].style.visibility=\"visible\";\n"
        "\n"
        "    if( layers )"
        "        svg.addEventListener(\n"
        "            \"wheel\",\n"
        "            function(e) {\n"
        "                var down = e.deltaY>0\n"
        "                var dir = !down && currIndex > 0 ? -1 : down && currIndex < maxIndex ? + 1 : 0;\n"
        "                if( dir ) {\n"
        "                    layers[currIndex].style.visibility=\"hidden\";\n"
        "                    layers[currIndex+=dir].style.visibility=\"visible\";\n"
        "                }\n"
        "            }\n"
        "        );\n"
        "</script>\n</body>\n</html>" << std::endl;
    }
    catch( const std::exception &ex ) {
        std::cerr << "error on processing:" << ex.what() << std::endl;
        return 102;
    }

    return 0;
}
