difference() {
    union() {
        translate([0,0,1])
            cube(size=2,center=true);
        translate([0,0,1.9])
            cylinder(r=0.5,h=1.1,$fn=16);
    }
    translate([0,0,-1])
        cylinder(r=0.2,h=5,$fn=8);
}
    