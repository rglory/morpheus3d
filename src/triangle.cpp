#include "triangle.hpp"

#include <algorithm>


std::ostream &operator<<( std::ostream &out, const Triangle &t )
{
    out << "[";
    for( const auto &p : t ) 
        out << " " << p;
    return out << " ] * " << t.norm();
}

Triangle::Triangle()
{
}

void Triangle::move( const Point3D &d )
{
    for( auto &p : m_array )
        p += d;
    calcNorm();
}

void Triangle::calcNorm()
{
    m_norm = ( m_array[1] - m_array[0] ) * ( m_array[2] - m_array[0] );
}

std::array<int,3> Triangle::signs( double z ) const
{
    std::array<int,3> r;
    std::transform( begin(), end(), r.begin(), [z]( const Point3D &p ) { return dcmp( p.z(), z ); } );
    return r;
}


bool Triangle::intersects( double z ) const
{
    auto ss = signs( z );
    if( ss[0] == ss[1] && ss[1] == ss[2] ) return false; // skip that does not intersect or fully lay on z
    return true;
}

Polygon Triangle::intersection( double z ) const
{
    Polygon p;
    auto ss = signs( z );

    for( int i = 0; i < 3; ++i ) {
        int ni = ( i + 1 ) % 3;
        if( ss[i] == ss[ni] ) continue;
        const Point3D &p1 = m_array[i];
        const Point3D &p2 = m_array[ni];
        double t = ( z - p1.z() ) / ( p2.z() - p1.z() );
        double x = p1.x() + ( p2.x() - p1.x() ) * t;
        double y = p1.y() + ( p2.y() - p1.y() ) * t;
        if( p.empty() || p.front() != Point2D{ x, y } ) {
            p.emplace_back( x, y );
            if( p.size() == 2 ) {
                auto v = p[0].get3d( z ) - p[1].get3d( z );
                if( ( v * norm() ).z() > 0 ) std::swap( p[0], p[1] );
                break;
            }
        }
    }
    //std::cout << "z= " << z << " poly " << p << " from " << *this << std::endl;

    return p;
}
