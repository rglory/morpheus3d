#include <iostream>

#include "../src/polygon.hpp"

int test( const Polygon &t, const Polygon &in, bool expectIn )
{
    if( in.isInside( t ) != expectIn ) {
        std::cout << "fail: " << t << " not "
                  << ( expectIn ? "insdie" : "outside" )
                  << " of " << in << std::endl;
        return 100;
    }
    return 0;
}

int testDots()
{
    Polygon p { { -1,-1 }, {-1,1}, {1,1}, {1,-1} };
    int rez = 0;
    rez |= test( Polygon{ { 0,0 } }, p, true );
    rez |= test( Polygon{ { -1,-1 } }, p, true );
    rez |= test( Polygon{ { -1,1 } }, p, true );
    rez |= test( Polygon{ { 1,1 } }, p, true );
    rez |= test( Polygon{ { 1,-1 } }, p, true );
    rez |= test( Polygon{ { -1,0 } }, p, true );
    rez |= test( Polygon{ { 1,0 } }, p, true );
    rez |= test( Polygon{ { 0,-1 } }, p, true );
    rez |= test( Polygon{ { 0,1 } }, p, true );
    rez |= test( Polygon{ { -2,0 } }, p, false );
    rez |= test( Polygon{ { 2,0 } }, p, false );
    rez |= test( Polygon{ { 0,-2 } }, p, false );
    rez |= test( Polygon{ { 0,2 } }, p, false );
    return rez;
}

int main()
{
    int rez = 0;
    rez |= testDots();
    return rez;
}
