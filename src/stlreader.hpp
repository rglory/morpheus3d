#ifndef STLREADER_HPP
#define STLREADER_HPP

#include "triangle.hpp"

#include <fstream>

class StlReader {
public:
    StlReader( const std::string &fname );

    bool next( Triangle &t );

private:
    void readText( Triangle &t );
    void readBin( Triangle &t );

    std::ifstream m_file;
    bool          m_bin;
};

#endif // STLREADER_HPP
