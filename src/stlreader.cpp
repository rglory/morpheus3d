#include "stlreader.hpp"

#include <sstream>

StlReader::StlReader( const std::string &fname ) :
    m_file( fname )
{
    if( !m_file )
        throw std::runtime_error( "cannot open file " + fname );
    char buffer[128];
    m_file.read( buffer, 6 );
    m_bin = std::string( buffer, 6 ) != "solid ";
    if( m_bin ) m_file.seekg( 84 );
    else m_file.ignore( 1024, '\n' );
}

bool StlReader::next( Triangle &t )
{
    if( m_bin ) readBin( t );
    else readText( t );
    return m_file.good();
}

void StlReader::readText( Triangle &t )
{
    size_t vertex = 0;
    while( m_file ) {
        std::string line;
        std::getline( m_file, line );
        std::istringstream in( line );
        std::string word;
        in >> word;
        if( word == "endfacet" ) break;
        if( word == "vertex" ) {
            double x,y,z;
            in >> x >> y >> z;
            if( vertex >= 3 )
                throw std::runtime_error( "too many vertexes" );
            t.set( vertex++, Point3D{ x, y, z } );
        }
    }
}

void StlReader::readBin( Triangle &t )
{
    float v = 0;
    for( int i = 0; i < 3; ++i )
        m_file.read( (char *)&v, sizeof( v ) );

    std::array<Point3D,3> points;
    for( auto &pt : points ) {
        float x, y, z;
        m_file.read( (char *)&x, sizeof( x ) );
        m_file.read( (char *)&y, sizeof( y ) );
        m_file.read( (char *)&z, sizeof( z ) );
        pt = Point3D{ x, y, z };
    }
    t = Triangle{ points.begin(), points.end() };
    uint16_t attr;
    m_file.read( (char *)&attr, sizeof( attr ) );
}

