#include "mesh.hpp"

#include "stlreader.hpp"

#include <map>
#include <set>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <functional>

//#define DEBUG_SLICE 1

Mesh::Mesh()
{
}

Triangle Mesh::getTriangle( size_t id ) const
{
    const auto &links = m_triangles[ id ];
    return Triangle{ links.begin(), links.end(), [this]( size_t pid )->Point3D { return m_points[pid].m_p; } };
}


size_t Mesh::getPoint( const Point3D &p, size_t tid )
{
    auto f = std::lower_bound( m_pointIdx.begin(), m_pointIdx.end(), p, [this]( size_t idx, const auto &ptr ) {
        return m_points[idx].m_p < ptr;
    } );
    if( f == m_pointIdx.end() || m_points[*f].m_p != p ) {
        auto id = m_points.size();
        f = m_pointIdx.insert( f, id );
        m_points.emplace_back( p );
    }
    m_points[*f].m_triangles.push_back( tid );
    return *f;
}

void Mesh::add( const Triangle &t )
{
    m_min = std::accumulate( t.begin(), t.end(), m_triangles.empty() ? t[0] : m_min, ::min );
    m_max = std::accumulate( t.begin(), t.end(), m_triangles.empty() ? t[0] : m_max, ::max );
    auto tid = m_triangles.size();
    m_triangles.emplace_back();
    std::transform( t.begin(), t.end(), std::begin( m_triangles.back() ),
                    std::bind( &Mesh::getPoint, this, std::placeholders::_1, tid ) );
}


void Mesh::load( const std::string &stl )
{
    StlReader r( stl );

    Triangle t;
    while( r.next( t ) ) {
        if( t[0] == t[1] || t[1]==t[2] || t[0] == t[2] ) // detect invalid triangles
            continue;
        add( t );
    }

    m_zIdx.resize( m_points.size() );
    std::iota( m_zIdx.begin(), m_zIdx.end(), 0 );
    std::sort( m_zIdx.begin(), m_zIdx.end(), [this]( size_t l, size_t r ) {
                   return m_points[l].m_p.z() < m_points[r].m_p.z();
               } );
}

void Mesh::print( std::ostream &out ) const
{
    out << "mesh " << m_triangles.size() << " triangles" << std::endl;
    for( size_t i = 0; i < m_triangles.size(); ++i ) {
        out << "\ttriangle " << i << "\t - " << getTriangle( i ) << std::endl;
    }
}

void Mesh::transfer( const Point3D &d )
{
    for( auto &t : m_points )
        t.m_p += d;

}

size_t getMove( const Polygon &p, size_t s )
{   
    const size_t start = s % p.size();
    size_t prev = ( start + 1 ) % p.size();
    double sumDist = dist( p[start], p[prev] );
    size_t steps = 1;
    for (; prev != start; steps++)
    {   
        const size_t next = ( prev + 1 ) % p.size();
        sumDist += dist( p[next], p[prev] );
        if( dcmp( sumDist, dist( p[next],  p[start] ) ) )
            break;
        prev = next;
    }
    return steps;
}
 
Polygon reduce( const Polygon &p )
{
    Polygon result;
    if( p.empty() )
        return result;
 
    const size_t s = getMove(p, 0);
    for ( size_t moved = 0; moved < p.size(); moved += getMove(p, s + moved) )
        result.emplace_back( p[(s + moved) % p.size()] );

    auto min = std::min_element( result.begin(), result.end() );
    if( min != result.begin() ) {
        Polygon tmp{ result.begin(), min };
        result.erase( result.begin(), min );
        result.insert( result.end(), tmp.begin(), tmp.end() );
    }
    return result;
}

Polygons Mesh::slice( double z ) const
{
    Polygons ps;

    std::vector<bool> processed( m_triangles.size(), false );
    for( size_t pt : m_zIdx ) {
        const PointData &pd = m_points[ pt ];
        if( dcmp( pd.m_p.z(), z ) == 1 )
            break;
        for( auto tid : pd.m_triangles ) {
            if( processed[ tid ] ) continue;
            processed[ tid ] = true;
            auto t = getTriangle( tid );
            auto p = t.intersection( z );
            if( p.size() ) {
#ifdef DEBUG_SLICE
                std::cout << "got z=" << dround(z) << " " << p << " from " << t << std::endl;
#endif
                ps.emplace_back( std::move( p ) );
            } 
        }
    }

    std::sort( ps.begin(), ps.end() );
    ps.erase( std::unique( ps.begin(), ps.end() ), ps.end() );

#ifdef DEBUG_SLICE
    std::cout << "after sort: " << ps << std::endl;
#endif

    Polygons polys;
    while( !ps.empty() ) {
        auto np =  std::move( ps.front() );
        ps.erase( ps.begin() );
        while( true ) {
            auto p = std::equal_range( ps.begin(), ps.end(), Polygon{ np.back() },
                                       []( const auto &p1, const auto &p2 ) {
                return p1.front() < p2.front();
            });
            if( p.first == p.second )
                break;
            auto it = p.first;
            if( np.size() > 1 ) {
                auto v = np[ np.size() - 2 ] - np.back();
                it = std::min_element( p.first, p.second, [v]( const auto &p1, const auto &p2 ) {
                    if( p1.size() != p2.size() ) // single point comes first
                        return p1.size() < p2.size();
                    auto v1 = p1.back() - p1.front();
                    auto v2 = p2.back() - p2.front();
                    // if multiple choices we want to turn left
                    return angleClockwise( v, v1 ) < angleClockwise( v, v2 );
                } );
            }

#ifdef DEBUG_SLICE
            std::cout << "for np " << np << " candidates " << Polygons{ p.first, p.second }
                      << " min - " << *it << std::endl;
#endif

            std::move( it->begin(), it->end(), std::back_inserter( np ) );
            ps.erase( it );
        }
        auto r = reduce( np );
#ifdef DEBUG_SLICE
        std::cout << "after reduce " << r << std::endl;
#endif
        polys.emplace_back( std::move( r ) );
    }
    std::sort( polys.begin(), polys.end(), []( const auto &p1, const auto &p2 ) {
        return p1.area() < p2.area();
    } );

    Polygons rez;
    while( !polys.empty() ) {
        auto pl = std::move( polys.back() );
        polys.pop_back();
        auto it = std::find_if( rez.rbegin(), rez.rend(), [&pl]( const auto &tpl ) { return tpl.isInside( pl ); } );
#ifdef DEBUG_SLICE
        if( it != rez.rend() )
            std::cout << "inside z =  " << z << " - " << pl << " is in " << *it << std::endl;
        else
            std::cout << "notinside z =  " << z << " - " << pl << std::endl;
#endif
        if( it == rez.rend() || ( pl.area() > 0 && it->isClockWise() != pl.isClockWise() ) )
            rez.emplace_back( std::move( pl ) );
    }
    return rez;
}
