#ifndef POINT2D_HPP
#define POINT2D_HPP

#include "point3d.hpp"

#include <vector>

class Point2D {
public:
    Point2D( double x, double y ) : m_x( x ), m_y( y ) {}
    Point2D() : Point2D( 0.0, 0.0 ) {}
    Point2D( const Point3D &p ) : Point2D( p.x(), p.y() ) {}

    double x() const { return m_x; }
    double y() const { return m_y; }
    double atan2() const { return ::atan2( m_y, m_x ); }

    Point3D get3d( double z ) const { return Point3D{ m_x, m_y, z }; }

    Point2D &operator-=( const Point2D &p );
    Point2D &operator+=( const Point2D &p );

private:
    double m_x;
    double m_y;
};

inline
std::ostream &operator<<( std::ostream &out, const Point2D &p )
{
    return out << '(' << dround( p.x() ) << ',' << dround( p.y() ) << ')';
}




bool operator<( const Point2D &p1, const Point2D &p2 );
bool operator<=( const Point2D &p1, const Point2D &p2 );
bool operator>( const Point2D &p1, const Point2D &p2 );
bool operator>=( const Point2D &p1, const Point2D &p2 );
bool operator==( const Point2D &p1, const Point2D &p2 );

inline
bool operator!=( const Point2D &p1, const Point2D &p2 )
{
    return !( p1 == p2 );
}

inline
Point2D &Point2D::operator-=( const Point2D &p )
{
    m_x -= p.m_x;
    m_y -= p.m_y;
    return *this;
}

inline
Point2D &Point2D::operator+=( const Point2D &p )
{
    m_x += p.m_x;
    m_y += p.m_y;
    return *this;
}

inline
Point2D operator-( const Point2D &p1, const Point2D &p2 )
{
    return Point2D{ p1.x() - p2.x(), p1.y() - p2.y() };
}

inline
double dist( const Point2D &p1, const Point2D &p2 )
{
    auto p = p1 - p2;
    return std::sqrt( p.x() * p.x() + p.y() * p.y() );
}

inline
double angleClockwise( const Point2D &p1, const Point2D &p2 )
{
    double d = p1.atan2() - p2.atan2();
    if( dcmp( d, 0 ) <= 0 ) d += 2 * M_PI;
    return d;
}

inline
Point2D operator+( const Point2D &p1, const Point2D &p2 )
{
    return Point2D{ p1.x() + p2.x(), p1.y() + p2.y() };
}

#endif // POINT2D_HPP
