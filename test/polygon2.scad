rotate([90,0,0])
linear_extrude( height = 1, center=true )
polygon( [[-0.5,0],[0.5,0],[0,1]] );

rotate([0,-90,0])
linear_extrude( height = 1, center=true )
polygon( [[0,-0.5],[0,0.5],[1,0]] );
