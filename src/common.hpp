#ifndef COMMON_HPP
#define COMMON_HPP

#include <cmath>
#include <algorithm>

constexpr double eps = 1e-10;

inline
int dcmp( double d1, double d2 )
{
    if( std::fabs( d1 - d2 ) <= eps ) return 0;
    return d1 < d2 ? -1 : 1;
}

inline
double dround( double d )
{
    if( dcmp( d, 0.0 ) == 0 ) return 0.0;
    return std::round( d / eps ) * eps;
}

template<typename Iterator, typename BinaryOp>
void apply2( Iterator b, Iterator e, BinaryOp op )
{
    for( auto i = b; i != e; ++i ) {
        auto n = i;
        if( ++n == e ) n = b;
        op( *i, *n );
    }
}

template<typename Iterator, typename TernaryOp>
void apply3( Iterator b, Iterator e, TernaryOp op )
{
    for( auto i = b; i != e; ++i ) {
        auto n = i;
        if( ++n == e ) n = b;
        auto nn = n;
        if( ++nn == e ) nn = b;
        op( *i, *n, *nn );
    }
}

template<typename Iterator, typename T, typename BinaryOp>
T accumulate2( Iterator b, Iterator e, T v, BinaryOp op )
{
    apply2( b, e, [&v,&op]( const auto &p1, const auto &p2 ) {
        v += op( p1, p2 );
    } );
    return v;
}

template<typename Iterator, typename T, typename TernaryOp>
T accumulate3( Iterator b, Iterator e, T v, TernaryOp op )
{
    apply3( b, e, [&v,&op]( const auto &p1, const auto &p2, const auto &p3 ) {
        v += op( p1, p2, p3 );
    } );
    return v;
}

template<typename Iterator, typename IteratorTo, typename BinaryOp>
void transform2( Iterator b, Iterator e, IteratorTo to, BinaryOp op )
{
    apply2( b, e, [&to,&op]( const auto &p1, const auto &p2 ) {
        *to++ = op( p1, p2 );
    } );
}

template<typename Iterator, typename IteratorTo, typename TernaryOp>
void transform3( Iterator b, Iterator e, IteratorTo to, TernaryOp op )
{
    apply3( b, e, [&to,&op]( const auto &p1, const auto &p2, const auto &p3 ) {
        *to++ = op( p1, p2, p3 );
    } );
}



#endif // COMMON_HPP
