#ifndef MESH_HPP
#define MESH_HPP

#include "triangle.hpp"
#include "point2d.hpp"

#include <map>

class Mesh {
public:
    Mesh();

    void load( const std::string &stl );
    void transfer( const Point3D &d );

    size_t size() const { return m_triangles.size(); }
    Point3D min() const { return m_min; }
    Point3D max() const { return m_max; }
    Polygons slice( double z ) const;
    void print( std::ostream &out ) const;

private:
    void add( const Triangle &t );
    Triangle getTriangle( size_t id ) const;
    size_t getPoint( const Point3D &p , size_t tid );

    typedef std::vector<size_t>  Ids;
    typedef std::array<size_t,3> Links;

    typedef std::vector<Links>   Triangles;
    
    struct PointData {
        PointData( Point3D p ) : m_p{ std::move( p ) } {}

        Point3D m_p;
        Ids     m_triangles;
    };

    typedef std::vector<PointData> Points;

    Triangles     m_triangles;
    Points        m_points;
    Ids           m_pointIdx;
    Ids           m_zIdx;
    Point3D       m_min;
    Point3D       m_max;
};

#endif // MESH_HPP
